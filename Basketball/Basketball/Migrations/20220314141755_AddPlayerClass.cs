﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Basketball.Migrations
{
    public partial class AddPlayerClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Height",
                table: "Persons",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JerseyNumber",
                table: "Persons",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Position",
                table: "Persons",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Trainer_TeamId",
                table: "Persons",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Weight",
                table: "Persons",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Persons_JerseyNumber",
                table: "Persons",
                column: "JerseyNumber",
                unique: true,
                filter: "[JerseyNumber] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_Trainer_TeamId",
                table: "Persons",
                column: "Trainer_TeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_Teams_Trainer_TeamId",
                table: "Persons",
                column: "Trainer_TeamId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Persons_Teams_Trainer_TeamId",
                table: "Persons");

            migrationBuilder.DropIndex(
                name: "IX_Persons_JerseyNumber",
                table: "Persons");

            migrationBuilder.DropIndex(
                name: "IX_Persons_Trainer_TeamId",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "Height",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "JerseyNumber",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "Position",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "Trainer_TeamId",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "Weight",
                table: "Persons");
        }
    }
}
