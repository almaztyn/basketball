using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Basketball.Enums;
using Basketball.Interfaces;

namespace Basketball.Models
{
    public class Team : IEntity<int>
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string City { get; set; }
        public Conference Conference { get; set; }
        public DateTime FoundedDate { get; set; } 
        public int NumberOfWins { get; set; }
        public int NumberOfLoss { get; set; }
        
        public virtual Trainer Trainer { get; set; }
        public virtual ICollection<Player> Players { get; set; } = new List<Player>();
    }
}