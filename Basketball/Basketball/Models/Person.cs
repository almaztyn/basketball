using System;
using System.ComponentModel.DataAnnotations;
using Basketball.Interfaces;

namespace Basketball.Models
{
    public class Person : IEntity<int>
    {
        public int Id { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}