using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Basketball.Enums;

namespace Basketball.Models
{
    public class Player : Person
    {
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        [Required]
        public int Height { get; set; }
        
        [Required]
        public int Weight { get; set; }

        [Required]
        public int JerseyNumber { get; set; }
        
        [Required]
        public Position Position { get; set; }
        public virtual Team Team { get; set; }
    }
}