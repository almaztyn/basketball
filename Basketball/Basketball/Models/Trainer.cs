using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Basketball.Models
{
    public class Trainer : Person
    {
        [Required]
        public int YearsOfWork { get; set; }
        
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        
        
        public virtual Team Team { get; set; }
    }
}