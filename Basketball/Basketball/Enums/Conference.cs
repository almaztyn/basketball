namespace Basketball.Enums
{
    public enum Conference
    {
        Eastern = 1,
        Western = 2,
    }
}