namespace Basketball.Interfaces
{
    public class IEntity <T>
    {
        public T Id { get; set; }
    }
}