﻿using System;
using System.Linq;
using Basketball.Enums;
using Basketball.Models;

namespace Basketball
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ApplicationDbContext())
            {
                Seed(db);
            }
        }
        
        public static void Seed(ApplicationDbContext db)
        {
            if (!db.Teams.Any())
            {
                var team1 = new Team()
                {
                    Name = "Bishkek Lakers", 
                    City = "Bishkek",
                    Conference = Conference.Eastern,
                    FoundedDate = new DateTime(2015,05,15),
                    NumberOfWins = 15,
                    NumberOfLoss = 8
                };

                var trainer1 = new Trainer()
                {
                    FirstName = "Almaz",
                    LastName = "Tynybekov",
                    DateOfBirth = new DateTime(1989,01,04),
                    YearsOfWork = 3,
                    Team = team1,
                };
                
                var player1 = new Player()
                {
                    FirstName = "John",
                    LastName = "Bacani",
                    DateOfBirth = new DateTime(1998,10,08),
                    Height = 198,
                    Weight = 85,
                    JerseyNumber = 9,
                    Position = Position.PG,
                    Team = team1,
                };
                
                var player2 = new Player()
                {
                    FirstName = "Barn",
                    LastName = "Samuelson",
                    DateOfBirth = new DateTime(1996,03,04),
                    Height = 190,
                    Weight = 82,
                    JerseyNumber = 2,
                    Position = Position.C,
                    Team = team1,
                };
                
                db.Trainers.Add(trainer1);
                db.Players.Add(player1);
                db.Players.Add(player2);
                db.SaveChanges();
            }
        }
    }
}